Заметка для развертывания проекта повторно


1) На сервер закинуть проект в любую удобную папку (Например в /home/project/)
---

Для добавления дополнительных доменов обновить файл .env в DOMAINS добавить домена через запятую без пробелов например:
```
DOMAINS=example.ru,www.example.ru,example111.ru,www.example111.ru
```


Для запуска проекта 
```bash
./start.sh
```

Если внесли правки в настройки nginx 

```bash
docker exec -it nginx-web nginx -s reload
```

Если нужно создать админа и не только 

```bash
docker-compose run <web> python manage.py createsuperuser
```


__Презентация__

[Ссылка на преентацию](https://gitlab.com/barmin/hackday-studworks/-/blob/master/docs/Презентация%20Studworks.pptx)

__Видео описание проекта__

https://www.youtube.com/watch?v=i7s_1OwQUiU
https://www.youtube.com/watch?v=jh133-fS4zc

__Ссылка на проект__

https://3dm.barm.in/
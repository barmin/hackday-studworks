from django.conf import settings
from django.contrib.auth import logout, authenticate, login
from django.contrib.auth.models import User
from django.shortcuts import render, redirect, get_object_or_404

# Create your views here.
from django.urls import reverse
from django.views.decorators.http import require_POST

from site_settings.models import University, Companies
from site_settings.utils.company import CompanyApi
from site_settings.utils.create_user import create_user
from user_profile.models import UserStudent, UserCompany


def profile_view(request):
    user = request.user
    if user.is_authenticated:
        return render(request, 'user_profile/profile.html', {'user': user, 'SUPPORT_EMAIL': settings.SUPPORT_EMAIL})
    return redirect(reverse('profile:login'))


def logout_view(request):
    logout(request)
    return redirect('/')


def forgotten_password(request):
    logout(request)
    return redirect('/')


def login_view(request):
    if request.user.is_authenticated:
        return redirect(reverse('profile:profile'))
    if request.method == 'POST':
        username = request.POST.get('email', None)
        password = request.POST.get('password', None)
        user = authenticate(request=request, username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                return redirect(request.GET.get('next', '/'))
            else:
                return render(request, 'user_profile/banned.html', status=401)
        else:
            return render(request, 'user_profile/unauthorized.html', status=401)
    return render(request, 'user_profile/login.html')


def registration_view(request):
    if request.user.is_authenticated:
        return redirect(reverse('profile:profile'))
    if request.method == 'POST':
        print(request.POST)
        is_student = request.POST.get('student') == 'Регистрация'
        is_company = request.POST.get('company') == 'Регистрация'
        if is_student or is_company:
            email = request.POST.get('email', None)
            password = request.POST.get('password', None)
            password_check = request.POST.get('password-check', None)
            organization = request.POST.get('organization', None)
            organization_custom = request.POST.get('organization-custom', None)
            user = create_user(request, email, password, password_check)
            if not type(user) == User:
                return render(request, 'user_profile/unauthorized.html', status=401)
            if user.is_active:
                login(request, user, backend='django.contrib.auth.backends.ModelBackend')
                if is_student:
                    if organization != 'None':
                        organization = get_object_or_404(University, id=organization)
                    else:
                        organization = University.objects.update_or_create(
                            name=organization_custom,
                            defaults={'name': organization_custom}
                        )[0]
                    profile = UserStudent()
                    profile.user = user
                    profile.university = organization
                    profile.save()
                elif is_company:
                    if organization != 'None':
                        organization = get_object_or_404(Companies, bin=organization)
                    else:
                        company = CompanyApi(organization_custom)
                        if company.status:
                            organization = Companies.objects.update_or_create(
                                bin=company.bin,
                                defaults={'name': company.name, 'bin': company.bin, 'address': company.address}
                            )[0]
                    profile = UserCompany()
                    profile.user = user
                    profile.company = organization
                    profile.save()
                return redirect(request.GET.get('next', '/'))
            else:
                return render(request, 'user_profile/banned.html', status=401)

    context = {}
    context['companies'] = Companies.objects.all()
    context['universities'] = University.objects.all()
    return render(request, 'user_profile/registration.html', context)


def edit_profile_view(request):
    user = request.user
    if user.is_authenticated:
        return render(request, 'user_profile/edit_profile.html', {'user': user})
    return render(request, 'user_profile/unauthorized.html', status=401)
from django.contrib import admin

# Register your models here.
from .models import UserCompany, UserStudent


class UserStudentAdmin(admin.ModelAdmin):
    pass
    # list_display = ['name', 'url', 'active']


admin.site.register(UserStudent, UserStudentAdmin)

class UserCompanyAdmin(admin.ModelAdmin):
    pass
    # list_display = ['name', 'url', 'active']


admin.site.register(UserCompany, UserCompanyAdmin)
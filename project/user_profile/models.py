from django.contrib.auth.models import User
from django.db import models
from ckeditor.fields import RichTextField



class UserStudent(models.Model):
    from site_settings.models import University
    avatar = models.ImageField('Аватар студента', upload_to='avatars/', blank=True, null=True)
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='student')
    university = models.ForeignKey(University, on_delete=models.SET_NULL, related_name='users', null=True)

    description = RichTextField('Обо мне', null=True, blank=True)
    attributes = models.TextField('Личные качества', null=True, blank=True)
    meta = models.TextField('Цель и стремления', null=True, blank=True)

    class Meta:
        verbose_name = "профиль студента"
        verbose_name_plural = "Профили студентов"


class Skills(models.Model):
    name = models.CharField('Название навыка', unique=True, max_length=120)
    description = RichTextField('Описание навыка', null=True, blank=True)

    class Meta:
        verbose_name = "навык"
        verbose_name_plural = "Навыки"


class UserSkills(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='skills')
    skill = models.ForeignKey(Skills, on_delete=models.CASCADE, related_name='users')

    class Meta:
        verbose_name = "навык пользователя"
        verbose_name_plural = "Навыки пользователей"


class Course(models.Model):
    from site_settings.models import Companies
    TYPE_ONLINE = 0
    TYPE_OFFLINE = 1
    TYPE_CHOICES = (
        (TYPE_ONLINE, 'Онлайн-курс'),
        (TYPE_OFFLINE, 'Оффлайн-курс'),
    )

    name = models.CharField('Название курса', max_length=120)
    description = RichTextField('Описание курса', null=True, blank=True)
    price = models.FloatField('Стоимость')
    place = models.CharField('Местоположение и тип проведения', max_length=200)
    type = models.IntegerField(choices=TYPE_CHOICES, default=TYPE_ONLINE, verbose_name='Тип курса')

    company = models.ForeignKey(Companies, on_delete=models.CASCADE, related_name='courses')

    class Meta:
        verbose_name = "курс"
        verbose_name_plural = "Курсы"


class UserCerts(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='certs')
    course = models.ForeignKey(Course, on_delete=models.CASCADE, related_name='users_who_course')
    cert_image = models.ImageField('Сертификат', upload_to='certs/', blank=True, null=True)
    show_cert = models.BooleanField('Отображать сертификат?', default=True)

    class Meta:
        verbose_name = "сертификат пользователя"
        verbose_name_plural = "Сертификаты пользователей"


class UserEducation(models.Model):
    from site_settings.models import University
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='education')
    university = models.ForeignKey(University, on_delete=models.CASCADE, related_name='users_who_study')
    start_date = models.DateField('Дата поступления', blank=True, null=True)
    end_date = models.DateField('Дата окончания', blank=True, null=True)

    class Meta:
        verbose_name = "образование пользователя"
        verbose_name_plural = "Образование пользователей"


class UserWork(models.Model):
    from site_settings.models import Companies
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='works')
    company = models.ForeignKey(Companies, on_delete=models.CASCADE, related_name='users_who_works')
    start_date = models.DateField('Дата поступления', blank=True, null=True)
    end_date = models.DateField('Дата окончания', blank=True, null=True)

    class Meta:
        verbose_name = "работа пользователя"
        verbose_name_plural = "Работа пользователей"


class UserCompany(models.Model):
    from site_settings.models import Companies
    avatar = models.ImageField('Аватар сотрудника', upload_to='avatars/', blank=True, null=True)
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='company')
    company = models.ForeignKey(Companies, on_delete=models.SET_NULL, related_name='users', null=True)

    class Meta:
        verbose_name = "профиль сотрудника компании"
        verbose_name_plural = "Профили сотрудников компании"

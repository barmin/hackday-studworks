"""project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.urls import path, re_path, include, reverse_lazy
from . import views


urlpatterns = [
    path('companies/', views.companies_page, name='companies'),
    path('courses/', views.courses_page, name='courses'),
    path('students/', views.students_page, name='students'),
    path('events/', views.events_page, name='events'),
    path('vacancy/<int:vacancy_id>', views.vacancy_detail_page, name='vacancy_detail'),
    path('companies/<int:company_id>', views.company_detail_page, name='company_detail'),
    path('', views.vacancy_page, name='vacancy'),
]

from django.http import HttpResponse
from django.shortcuts import render, redirect


# Create your views here.
from django.urls import reverse


def vacancy_page(request):
    context = {}
    return render(request, 'internship/vacancy.html', context)


def vacancy_detail_page(request, vacancy_id):
    context = {}
    print(vacancy_id)
    return render(request, 'internship/vacancy_detail.html', context)


def companies_page(request):
    context = {}
    return render(request, 'internship/companies.html', context)
    # return redirect(reverse('profile:profile'))


def company_detail_page(request, company_id):
    context = {}
    print(company_id)
    return render(request, 'internship/company_detail.html', context)
    # return redirect(reverse('profile:profile'))


def courses_page(request):
    context = {}
    return render(request, 'internship/courses.html', context)
    # return redirect(reverse('profile:profile'))


def students_page(request):
    context = {}
    return render(request, 'internship/students.html', context)
    # return redirect(reverse('profile:profile'))


def events_page(request):
    context = {}
    return render(request, 'internship/events.html', context)
    # return redirect(reverse('profile:profile'))



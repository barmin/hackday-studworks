"""project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
import os

from django.conf import settings
from django.contrib import admin
from django.http import HttpResponse
from django.urls import path, include, re_path, reverse_lazy
from django.views.generic import RedirectView
from django.conf.urls.static import static
from django.contrib.auth import views as auth_views

import internship.views
from project.views import index_page

admin.site.site_header = 'StudWorks админ. панель'
admin.site.index_title = 'Управление разделами сайта'
admin.site.site_title = 'StudWorks админ. панель'


def update_university(request):
    from site_settings.models import University
    from site_settings.utils.univeristy_info import university_json
    for item in university_json:
        University.objects.update_or_create(
            name=item['name'],
            defaults={'name': item['name']}
        )
    return HttpResponse('ok')


urlpatterns = [
    path('profile/', include(('user_profile.urls', 'profile'), namespace='profile')),
    path('internship/', include(('internship.urls', 'internship'), namespace='internship')),
    path('api/', include(('api.urls', 'api'), namespace='api')),

    path('password_reset_done/', auth_views.PasswordResetDoneView.as_view(
        template_name='user_profile/password_reset_done.html'
    ), name='password_reset_done'),
    re_path(r'password_reset_confirm/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
            auth_views.PasswordResetConfirmView.as_view(
                template_name='user_profile/password_reset_confirm.html'
            ), name='password_reset_confirm'),
    path('password_reset_complete/', auth_views.PasswordResetCompleteView.as_view(
        template_name='user_profile/password_reset_complete.html'
    ), name="password_reset_complete"),
    path('password_reset/', auth_views.PasswordResetView.as_view(
        template_name="user_profile/password_reset.html",
        html_email_template_name="email_templates/password_reset_email.html",
        success_url=reverse_lazy('password_reset_done'),
    ), name='password_reset'),

    path('admin/', admin.site.urls),
    path('update_university/', update_university),
    path('', index_page, name='index'),

]

if settings.DEBUG:
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns

    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += [
        path('favicon.ico', RedirectView.as_view(url=settings.STATIC_URL + 'content/images/favicon.ico'))
    ]
else:
    import django.views.static

    urlpatterns += [
        re_path(r'^static/(?P<path>.*)$', django.views.static.serve,
                {'document_root': settings.STATIC_ROOT, 'show_indexes': settings.DEBUG})
    ]
"""
Django settings for project project.

Generated by 'django-admin startproject' using Django 3.0.

For more information on this file, see
https://docs.djangoproject.com/en/3.0/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/3.0/ref/settings/
"""

import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import random
import string

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/3.0/howto/deployment/checklist/

if 'DJANGO_SECRET_KEY' in os.environ:
    SECRET_KEY = os.environ['DJANGO_SECRET_KEY']
else:
    SECRET_KEY = ''.join([random.SystemRandom().choice(string.printable) for i in range(50)])

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['*']

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.humanize',

    'rest_framework',
    'rest_framework.authtoken',

    'ckeditor',
    'ckeditor_uploader',
    'axes',

    'api',
    'site_settings',
    'user_profile',
    'internship',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'axes.middleware.AxesMiddleware',
]

ROOT_URLCONF = 'project.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
            'libraries': {
                }
        },
    },
]

WSGI_APPLICATION = 'project.wsgi.application'

# Database
# https://docs.djangoproject.com/en/3.0/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.postgresql_psycopg2',
#         'NAME': 'db',
#         'USER': 'user',
#         'PASSWORD': 'password',
#         'HOST': 'db',
#     }
# }

# Password validation
# https://docs.djangoproject.com/en/3.0/ref/settings/#auth-password-validators

AUTHENTICATION_BACKENDS = [
    # AxesBackend should be the first backend in the AUTHENTICATION_BACKENDS list.
    'axes.backends.AxesBackend',

    # Django ModelBackend is the default authentication backend.
    'django.contrib.auth.backends.ModelBackend',
]

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

LANGUAGE_CODE = 'ru'

TIME_ZONE = 'Etc/GMT-9'
# TODONE: заменить на нужный город для заказчика https://en.wikipedia.org/wiki/List_of_tz_database_time_zones

USE_I18N = True

USE_L10N = True

USE_TZ = True

# Static files (CSS, JavaScript, Images)

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static')

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')


CKEDITOR_UPLOAD_PATH = "uploads/"


CART_SESSION_ID = 'cart'
CART_DELIVERY_SESSION_ID = 'delivery_method'
CART_PAY_METHOD_SESSION_ID = 'pay_method'



# Почта
EMAIL_HOST = 'smtp.yandex.ru'
EMAIL_HOST_USER = 'studworks@barm.in'
EMAIL_HOST_PASSWORD = '4mW-Rm7-Ha9-48S'
EMAIL_PORT = 587
EMAIL_USE_TLS = True

EMAIL_NAME_USER = f'StudWorks <{EMAIL_HOST_USER}>'  # для отправки вместо email имени пользователя
DEFAULT_FROM_EMAIL = EMAIL_NAME_USER
ADMIN_EMAIL = 'studworks@barm.in'
SUPPORT_EMAIL = 'studworks@barm.in'

# Защита от брутфорса
AXES_FAILURE_LIMIT = 10
AXES_LOCKOUT_TEMPLATE = 'user_profile/try_count.html'
AXES_RESET_ON_SUCCESS = True

import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration

sentry_sdk.init(
    dsn="https://bc0cc1463a5042849ca4bd490267776d@o358840.ingest.sentry.io/5224188",
    integrations=[DjangoIntegration()],

    # If you wish to associate users to errors (assuming you are using
    # django.contrib.auth) you may enable sending PII data.
    send_default_pii=True
)

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.TokenAuthentication',
    )
}


EDU_EMAIL_DOMAINS = [
    'enu.kz', 'kazguu.kz', 'amu.kz', 'fin-academy.kz', 'kuef.kz', 'apa.kz', 'nu.edu.kz',
    'art-oner.kz', 'conservatoire.kz', 'kaznpu.kz', 'kaznau.kz', 'kaznmu.kz', 'satbayev.university', 'kaznu.kz',
    'kazmkpu.kz', 'agakaz.kz', 'aipet.kz', 'kazatk.kz', 'ablaikhan.kz', 'narxoz.kz', 'kbtu.kz',
    'mok.kz', 'iitu.kz', 'kimep.kz', 'etu.edu.kz', 'almau.edu.kz', 'vuzkunaeva.kz', 'kazadi.kz', 'kazetu.kz',
    'dku.kz', 'cu.edu.kz', 'kazuniver.kz', 'turan-edu.kz', 'symbat.kz', 'uib.kz', 'cau.kz', 'kazmuno.kz', 'ksph.kz'
]


OPENDATA_API_KEY = 'b41811f9cf464b1d9d5e91ac79be587e'
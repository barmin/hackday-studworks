from django.contrib import admin

from .models import University, Companies


class CompaniesAdmin(admin.ModelAdmin):
    pass
    # list_display = ['name', 'url', 'active']


admin.site.register(Companies, CompaniesAdmin)


class UniversitiesAdmin(admin.ModelAdmin):
    pass
    # list_display = ['name', 'url', 'active']


admin.site.register(University, UniversitiesAdmin)

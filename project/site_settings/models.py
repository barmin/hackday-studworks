from django.contrib.auth.models import User
from django.db import models


class University(models.Model):
    name = models.CharField('Наименование учебного заведения', max_length=200)

    class Meta:
        verbose_name = "университет"
        verbose_name_plural = "Университеты"


class Companies(models.Model):
    avatar = models.ImageField('Аватар компании', upload_to='companies/', blank=True, null=True)
    small_descripton = models.CharField('Краткое описание компании', max_length=120, blank=True, null=True)
    url = models.URLField('Ссылка на организацию', blank=True, null=True)
    bin = models.CharField('БИН', primary_key=True, max_length=12, editable=False)
    address = models.CharField('Адрес организации', max_length=500)
    name = models.CharField('Наименование ', max_length=200)


    class Meta:
        verbose_name = "компания"
        verbose_name_plural = "Компании"


class Events(models.Model):
    image = models.ImageField('Изображение события', upload_to='companies/', blank=True, null=True)
    small_descripton = models.CharField('Краткое описание события', max_length=120, blank=True, null=True)
    url = models.URLField('Ссылка', blank=True, null=True)
    organizer = models.ForeignKey(Companies, on_delete=models.SET_NULL, null=True)

    class Meta:
        verbose_name = "компания"
        verbose_name_plural = "Компании"


from django.contrib.auth.models import User
from django.db import IntegrityError

from site_settings.utils.email_sender import send_email_user_registered


def create_user(request, email, password, password_check, first_name='', last_name=''):
    if password != password_check:
        return {"error": "Пароли не совпадают"}
    try:
        user = User.objects.create_user(username=email,
                                        email=email,
                                        password=password)
        user.first_name = first_name
        user.last_name = last_name
        user.save()
        send_email_user_registered(request, user)
        return user
    except IntegrityError as e:
        if e.args[0] == 'UNIQUE constraint failed: auth_user.username':
            return {"error": "Пользователь с такими данными уже существует! "
                             "Если вы забыли свой пароль воспользуйтесь формой востановления пароля!"}
    except Exception as e:
        print(e)
    return {"error": "Произошла неизвестная ошибка! "}

# def generate_username(email)

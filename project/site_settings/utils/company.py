import requests

bin = '0008400010861'


class CompanyApi:
    def __init__(self, bin):
        company_info = {}
        self.status = False
        try:
            company_info = requests.get('https://stat.gov.kz/api/juridical/?bin={}&lang=ru'.format(bin), verify=False)
            company_info = company_info.json()['obj']
            self.status = True
        except:
            print('Произошла ошибка при получении организации')
        self.name = company_info.get('name')
        self.bin = company_info.get('bin')
        self.address = company_info.get('katoAddress')



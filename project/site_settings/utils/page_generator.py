def get_pages(start_page, end_page, current_page):
    if not current_page:
        current_page = 1
    pages = []
    if end_page < 7:
        for p in range(1, end_page+1):
            pages.append(p)
        return pages
    if current_page < 5:
        if current_page < 3:
            for p in range(1, 5):
                pages.append(p)
        else:
            for p in range(1, current_page + 3):
                pages.append(p)
        if end_page > 7:
            pages.append('...')
        pages.append(end_page)
        return pages
    if current_page < end_page - 3:
        pages.append(1)
        pages.append('...')
        pages.append(current_page-2)
        pages.append(current_page-1)
        pages.append(current_page)
        pages.append(current_page+1)
        pages.append(current_page+2)
        pages.append('...')
        pages.append(end_page)
        return pages
    elif current_page < end_page - 1:
        pages.append(1)
        pages.append('...')
        for p in range(current_page-2, end_page+1):
            pages.append(p)
        return pages
    elif current_page > end_page - 2:
        pages.append(1)
        pages.append('...')
        for p in range(end_page-3, end_page+1):
            pages.append(p)
        return pages
    raise Exception('Произошла ошибка при генерации страниц')

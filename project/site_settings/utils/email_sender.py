# html_message = render_to_string('mail_template.html', {'cart': cart, 'number': order.id_for_user, 'host': request.get_host()})
# plain_message = strip_tags(html_message)
# send_mail('Ваш заказ на сайте ' + request.get_host(), plain_message,
#       settings.EMAIL_NAME_USER, [order.email], fail_silently=False, html_message=html_message)
#
# html_message = render_to_string('admin_mail_template.html', {'cart': cart, 'order': order, 'host': request.get_host()})
# plain_message = strip_tags(html_message)
# send_mail('Новый заказ на сайте ' + request.get_host(), plain_message,
#       settings.EMAIL_NAME_USER, [settings.ADMIN_EMAIL], fail_silently=False, html_message=html_message)
from django.conf import settings
from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.utils.html import strip_tags


# def send_email_order_created(request, order):
#     html_message = render_to_string('email_templates/order_created.html', {'order': order, 'request': request})
#     plain_message = strip_tags(html_message)
#     send_mail('Ваш заказ на сайте ' + request.get_host(), plain_message,
#           settings.EMAIL_NAME_USER, [order.email], fail_silently=False, html_message=html_message)
#     html_message = render_to_string('email_templates/order_created_admin.html', {'order': order, 'request': request})
#     plain_message = strip_tags(html_message)
#     send_mail('Новый заказ на сайте ' + request.get_host(), plain_message,
#           settings.EMAIL_NAME_USER, [settings.ADMIN_EMAIL], fail_silently=False, html_message=html_message)
#

def send_email_user_registered(request, user):
    html_message = render_to_string('email_templates/registration_finish.html', {'user': user, 'request': request})
    plain_message = strip_tags(html_message)
    send_mail('Успешная регистрация на сайте ' + request.get_host(), plain_message,
          settings.EMAIL_NAME_USER, [user.email], fail_silently=False, html_message=html_message)
